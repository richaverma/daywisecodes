﻿using Microsoft.AspNetCore.Mvc;
using richa_practiceproject_25aug.Interfaces;
using richa_practiceproject_25aug.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace richa_practiceproject_25aug.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeacherController : ControllerBase
    {
        IteacherService iteacherservice; 

        public TeacherController(IteacherService teacherservice)
        {
            iteacherservice = teacherservice; 
        }

        // GET: api/<TeacherController>
        [HttpGet]
        public List<Teachers> Get()
        {
            return iteacherservice.getTeachers(); 
        }

        // GET api/<TeacherController>/5
        [HttpGet("{id}")]
        public Teachers Get(int id)
        {
            return iteacherservice.getSingleTeacher(id);
        }

        // POST api/<TeacherController>
        [HttpPost]
        public List<Teachers> Post([FromBody] Teachers obj)
        {
            return iteacherservice.add(obj);
        }

        // PUT api/<TeacherController>/5
        [HttpPut("{id}")]
        public List<Teachers> Put(Teachers obj)
        {
            return iteacherservice.add(obj);
        }

        // DELETE api/<TeacherController>/5
        [HttpDelete("{id}")]
        public List<Teachers> Delete(int id)
        {
            return iteacherservice.delete(id);
        }
    }
}
