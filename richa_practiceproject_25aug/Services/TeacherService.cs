﻿using richa_practiceproject_25aug.Interfaces;
using richa_practiceproject_25aug.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace richa_practiceproject_25aug.Services
{
    public class TeacherService : IteacherService
    {
        public List<Teachers> teacher = new List<Teachers>();

        public TeacherService()
        {
            Teachers obj = new Teachers();
            for(int i = 1; i<=10; i++)
            {
                obj.id = i;
                obj.age = 40 + i;
                obj.name = "Teacher" + i;
                obj.dept = "ECE";
                teacher.Add(obj);
            }
        }
        public List<Teachers> add(Teachers obj)
        {
            teacher.Add(obj);
            return teacher;
        }

        public List<Teachers> delete(int id)
        {
            teacher.RemoveAll(x => x.id == id);
            return teacher; 
        }

        public Teachers getSingleTeacher(int id)
        {
            return teacher.SingleOrDefault(x => x.id == id);
        }

        public List<Teachers> getTeachers()
        {
            return teacher;
        }
    }
}
