﻿using richa_practiceproject_25aug.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace richa_practiceproject_25aug.Interfaces
{
    public interface IteacherService
    {
        public List<Teachers> getTeachers();
        public Teachers getSingleTeacher(int id);
        public List<Teachers> delete(int id);
        public List<Teachers> add(Teachers teacher);
    }
}
