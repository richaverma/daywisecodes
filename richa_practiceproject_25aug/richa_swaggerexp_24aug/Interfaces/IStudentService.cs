﻿using richa_swaggerexp_24aug.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace richa_swaggerexp_24aug.Interfaces
{
    public interface IStudentService
    {
        List<Student> Gets();
        Student Get(int id);
        List<Student> Save(Student student);

        List<Student> Delete(int id);

    }
}
