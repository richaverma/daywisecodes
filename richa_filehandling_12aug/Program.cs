﻿using System;
using System.IO;

namespace richa_filehandling_12aug
{
    class Program
    {
        static void Main(string[] args)
        {
            filewrite wr = new filewrite();
            wr.writedata();
        }
        class filewrite
        {
            public void writedata()
            {
                FileStream fs = new FileStream("e:\\samplefile.txt", FileMode.Append, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fs);
                Console.WriteLine("Enter the text which you want to write in the file");
                String str = Console.ReadLine();
                sw.WriteLine(str);
                sw.Flush();
                sw.Close();
                fs.Close();
            }
        }
    }
}