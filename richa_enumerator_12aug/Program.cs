﻿using System;
using System.Collections;
namespace richa_enumerator_12aug
{
    class Program
    {
        static void Main(string[] args)
        {
            ArrayList list = new ArrayList();
            list.Add(123);
            list.Add("Test");
            list.Add(2.39);


            IEnumerator ienum = list.GetEnumerator();
            string msg = "";
            while (ienum.MoveNext())
            {
                msg += ienum.Current.ToString() + "\n"; 
            }

            Console.Write(msg);
        }
    }
}
