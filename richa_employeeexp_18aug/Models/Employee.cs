﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace richa_employeeexp_18aug.Models
{
    public class Employee
    {
        public int EmpId { get; set; }
        public string  EmpName { get; set; }

        public string Department { get; set; }

        public int age { get; set; }

        public int gender { get; set; }
    }
}
