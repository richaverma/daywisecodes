﻿using System;

namespace richa_classexp_11aug
{
    class Program
    {
        static void Main(string[] args)
        {
            cars carobj = new cars();
            carobj.display();
            carobj.displaycar();

            students studentobj = new students();
            studentobj.display();
            studentobj.displaystudent();

            Movies moviesobj = new Movies();
            moviesobj.displaymovies();
            moviesobj.displaytheatre(); 
        }

        class vehicle
        {
            public void display()
            {
                Console.WriteLine("It is a vehicle");
            }
        }

        class cars : vehicle
        {
            public void  displaycar()
            {
                Console.WriteLine("It is a car");
            }
        }

       class Class
        {
            public void display()
            {
                Console.WriteLine("This is a Class");
            }
        }

        class students : Class
        {
            public void displaystudent()
            {
                Console.WriteLine("This is a student class");
            }
        }

        class theatre
        {
            public void displaytheatre()
            {
                Console.WriteLine("This is a theatre class");
            }
        }
        class Movies: theatre
        {
            public void displaymovies()
            {
                Console.WriteLine("This is a movie class");
            }
        }

        class playstore
        {
            public void displayplaystore()
            {
                Console.WriteLine("This is the playstore class");
            }
        }

        class apps : playstore
        {
            public void displayapps()
            {
                Console.WriteLine("This is apps class");
            }
        }

        class library
        {
            public void displaylib()
            {
                Console.WriteLine("This is library class"); 
            }
        }

        class books : library
        {
            public void displaylibrary()
            {
                Console.WriteLine("This is library class");
            }
        }

        class airport
        {
            public void displayairport()
            {
                Console.WriteLine("This is airport class");
            }
        }

        class aeroplane : airport { 
        public void displayaeroplane()
            {
                Console.WriteLine("This is aeroplane class");
            }
        }

    }
}
