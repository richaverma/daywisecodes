﻿namespace richa_datetime_10aug
{
    internal class Program
    {
        static void Main(string[] args)
        {
           Console.WriteLine(DateTime.Now);
            //  Console.WriteLine(DayOfWeek);
            int[] arr = new int[] { 1, 2, 3 };
            //LINQ
            string[] mystr = { "tejashwin", "yash", "shantan" };
            var result = Array.Find(mystr, mystr => mystr == "shantan");
            Console.WriteLine(result);  
            
            cars carobj = new cars();
            carobj.display();
            carobj.displaycars();
        }

        class vehicle
        {
            public void display()
            {
                Console.WriteLine("This is vehicle class");
            }
        }

        class cars : vehicle
        {
            public void displaycars()
            {
                Console.WriteLine("This is a car class") ;
            }
        }
    }
}