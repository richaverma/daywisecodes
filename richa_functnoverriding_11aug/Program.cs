﻿using System;

namespace richa_functnoverriding_11aug
{
    class Program
    {
        static void Main(string[] args)
        {
            car carobj = new car();
            carobj.display();
        }

        abstract class vehicle
        {
            public abstract void display();
        }

        class car : vehicle
        {
            public override void display()
            {
               
                Console.WriteLine("This functn is overridden in child class");
            }
        }
    }
}
