﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using richa_studentrecord_19aug.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace richa_studentrecord_19aug.Controllers
{
    public class HomeController : Controller
    {
        private static IList<Students> student = new List<Students>
        {
            new Students{id=Guid.NewGuid(),name="Foe"},
            new Students{id=Guid.NewGuid(),name="Friend"}
        };
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }
        public  ViewResult Details()
        {
            ViewData["Heading"] = "This is Details page";
            ViewBag.info = "Details of the student is below : ";
            Students obj = new Students()
            {
                name = "Shawn" ,
                id = System.Guid.NewGuid()
            };
            ViewBag.student = obj;
            return View();
        }

        public IActionResult DetailList()
        {
            ViewData["Heading"] = "This is Students List Page";
            
           
            return View(student);
        }

        public ViewResult Singlestud()
        {
            ViewData["Heading"] = "This is Student Page";
            return View();
        }

        public ViewResult Addstud()
        {
            ViewData["Heading"] = "This is Add Student Page";
            return View();
        }
        [HttpPost]
        public ActionResult Addstud(Students input)
        {

            student.Add(new Students { name = input.name, id = Guid.NewGuid() });
            return  RedirectToAction("DetailList");


        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
