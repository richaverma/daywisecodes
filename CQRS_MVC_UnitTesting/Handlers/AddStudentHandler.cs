﻿using CQRS_MVC_UnitTesting.Commands;
using CQRS_MVC_UnitTesting.DataAccess;
using CQRS_MVC_UnitTesting.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CQRS_MVC_UnitTesting.Handlers
{
    public class AddStudentHandler : IRequestHandler<AddStudent, List<StudentTable>>
    {
       private readonly IDataAccess _data;
        public AddStudentHandler(IDataAccess data)
        {
            _data = data;
        }
        public Task<List<StudentTable>> Handle(AddStudent request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_data.addStudent(request.id , request.name , request.branch));
        }
    }
}
