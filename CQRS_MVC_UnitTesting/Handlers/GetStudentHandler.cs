﻿using CQRS_MVC_UnitTesting.DataAccess;
using CQRS_MVC_UnitTesting.Models;
using CQRS_MVC_UnitTesting.Queries;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CQRS_MVC_UnitTesting.Handlers
{
    public class GetStudentHandler : IRequestHandler<GetStudent, StudentTable>
    {
        private readonly IDataAccess _data;
        public GetStudentHandler(IDataAccess data)
        {
            _data = data;
        }
        public Task<StudentTable> Handle(GetStudent request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_data.getStudent(request.id));
        }
    }
}
