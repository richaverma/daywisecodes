﻿using CQRS_MVC_UnitTesting.DataAccess;
using CQRS_MVC_UnitTesting.Models;
using CQRS_MVC_UnitTesting.Queries;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CQRS_MVC_UnitTesting.Handlers
{
    public class GetStudentsHandler : IRequestHandler<GetStudents, List<StudentTable>>
    {
        private readonly IDataAccess _data ;
        public GetStudentsHandler(IDataAccess data)
        {
            _data = data;
        }
        public Task<List<StudentTable>> Handle(GetStudents request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_data.getStudents());
        }
    }
}
