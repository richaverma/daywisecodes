﻿using CQRS_MVC_UnitTesting.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CQRS_MVC_UnitTesting.Commands
{
    public class AddStudent : IRequest<List<StudentTable>>
    {
        public string name;
        public int id;
        public string branch;
    }
}
