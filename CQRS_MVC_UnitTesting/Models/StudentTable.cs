﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CQRS_MVC_UnitTesting.Models
{
    public partial class StudentTable
    {   [Required]
        public int StudentId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Branch { get; set; }
    }
}
