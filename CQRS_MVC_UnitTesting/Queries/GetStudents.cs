﻿using CQRS_MVC_UnitTesting.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CQRS_MVC_UnitTesting.Queries
{
    public class GetStudents : IRequest<List<StudentTable>>
    {
    }
}
