﻿using CQRS_MVC_UnitTesting.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CQRS_MVC_UnitTesting.DataAccess
{
    public class Dataccess : IDataAccess
    {
        public List<StudentTable> students = new List<StudentTable>()
        {
            new StudentTable{StudentId =1 , Name ="Richa" , Branch="ECE"} , 
            new StudentTable{StudentId =2 , Name ="Riddhi" , Branch="ECE"}
        };
        public List<StudentTable> addStudent(int id, string name, string branch)
        {
            students.Add(new StudentTable { StudentId = id, Name = name, Branch = branch });
            return students;
        }

        public StudentTable getStudent(int id)
        {
            return students.SingleOrDefault(o => o.StudentId == id);
        }

        public List<StudentTable> getStudents()
        {
            return students;
        }
    }
}
