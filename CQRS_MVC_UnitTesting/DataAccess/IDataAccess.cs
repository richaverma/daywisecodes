﻿using CQRS_MVC_UnitTesting.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CQRS_MVC_UnitTesting.DataAccess
{
    public interface IDataAccess
    {
        public List<StudentTable> addStudent(int id, string name, string branch);
        public List<StudentTable> getStudents();

        public StudentTable getStudent(int id);
    }
}
