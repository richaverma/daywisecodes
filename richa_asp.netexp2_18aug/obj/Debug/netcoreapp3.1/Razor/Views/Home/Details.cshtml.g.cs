#pragma checksum "C:\Users\eystack13\source\gitrepos\richa_asp.netexp2_18aug\Views\Home\Details.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "cb4ee887901ca4b01aa1adbc5eea4d9fb576d6a1"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Details), @"mvc.1.0.view", @"/Views/Home/Details.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\eystack13\source\gitrepos\richa_asp.netexp2_18aug\Views\_ViewImports.cshtml"
using richa_asp.netexp2_18aug;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\eystack13\source\gitrepos\richa_asp.netexp2_18aug\Views\_ViewImports.cshtml"
using richa_asp.netexp2_18aug.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"cb4ee887901ca4b01aa1adbc5eea4d9fb576d6a1", @"/Views/Home/Details.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"502629f85efe097cf7cacaa400174255a4d8124d", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Details : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("<div class=\"text-center\">\r\n    <h1 class=\"display-4\">Welcome</h1>\r\n    \r\n");
#nullable restore
#line 4 "C:\Users\eystack13\source\gitrepos\richa_asp.netexp2_18aug\Views\Home\Details.cshtml"
          
           var student = ViewData["Student"] as richa_asp.netexp2_18aug.Models.Students ;
               

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    <div>\r\n         <p> Student Id : ");
#nullable restore
#line 9 "C:\Users\eystack13\source\gitrepos\richa_asp.netexp2_18aug\Views\Home\Details.cshtml"
                     Write(student.studentID);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n        <p> Student Name : ");
#nullable restore
#line 10 "C:\Users\eystack13\source\gitrepos\richa_asp.netexp2_18aug\Views\Home\Details.cshtml"
                      Write(student.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n        <p> Student Section : ");
#nullable restore
#line 11 "C:\Users\eystack13\source\gitrepos\richa_asp.netexp2_18aug\Views\Home\Details.cshtml"
                         Write(student.sec);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n        <p>Student Gender : ");
#nullable restore
#line 12 "C:\Users\eystack13\source\gitrepos\richa_asp.netexp2_18aug\Views\Home\Details.cshtml"
                       Write(student.gender);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n    </div>\r\n    \r\n    </div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
