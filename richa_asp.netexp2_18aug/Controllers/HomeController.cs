﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using richa_asp.netexp2_18aug.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace richa_asp.netexp2_18aug.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

     /*   public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
     */
        public IActionResult Index()
        {
            ViewData["title"] = "This is title";
            ViewBag.someexample = "Example";
            return View();
        }

        public ViewResult Details()
        {
            Students obj = new Students()
            {
                studentID = 1,
                Name = "Richa",
                gender = "Female",
                sec = 'A'
            };
            ViewData["Student"] = obj;
            return View();
        }
        public IActionResult Privacy()
        {
            return View();
        }

      /*  [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }*/
    }
}
