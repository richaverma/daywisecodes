﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace richa_employee_crud.Models
{
    public class Employee
    {
        public string Name { get; set; }
        public Guid Id { get; set; }
        public string Phone { get; set; }
        public string Dept { get; set; }
        public DateTime DOB { get; set; }
    }
}
