﻿using System;

namespace richa_generics2_12aug
{
    class Program
    {
        public class someclass
        {
            public void genericmethod<T>(T param1, T param2)
            {
                Console.WriteLine($"Parameter 1 : {param1} : Prameter 2 : {param2}");
            }
        }


        static void Main(string[] args)
        {
            Console.WriteLine("Generics Method Example");
            someclass s = new someclass();
            //while calling the method we need to specify the datatype
            s.genericmethod<int>(10, 10);
            s.genericmethod<String>("Hii", "There");
            ;
        }
    }
}
