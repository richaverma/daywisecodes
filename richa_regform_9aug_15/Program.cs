﻿using System;

namespace richa_regform_9aug_15
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter your details for registration");
            String name, id;
            char gender;
            DateTime dob;
            Console.WriteLine("Enter your name");
            name = Console.ReadLine();
            Console.WriteLine("Enter your id");
            id = Console.ReadLine();
            Console.WriteLine("Enter your gender");
            gender = Convert.ToChar(Console.ReadLine());
            Console.WriteLine("Enter your date of birth");
            dob = Convert.ToDateTime(Console.ReadLine());
            Console.WriteLine("Your details are :");
            Console.WriteLine(name);
            Console.WriteLine(id);
            Console.WriteLine(gender);
            Console.WriteLine(dob);
        }
    }
}
