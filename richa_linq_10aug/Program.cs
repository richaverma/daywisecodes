﻿using System;
using System.Linq; 
namespace richa_linq_10aug
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] name = { "John", "Peter", "David" };
            string check1 = "Peter";
            string check2 = "Richa";
            var result = Array.Find(name, name => check1 == name);
            Console.WriteLine(result);
            result = Array.Find(name, name => check2 == name);
            Console.WriteLine(result);
        }
    }
}
