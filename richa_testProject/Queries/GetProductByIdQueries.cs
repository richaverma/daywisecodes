﻿using MediatR;
using richa_groceryproject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace richa_groceryproject.Queries
{
    public class GetProductByIdQueries :IRequest<Product>
    {
        public int id { get; set; }
    }
}
