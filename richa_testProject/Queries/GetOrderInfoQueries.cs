﻿using MediatR;
using richa_groceryproject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace richa_groceryproject.Queries
{
    public class GetOrderInfoQueries : IRequest<List<ProductOrder>>
    {
        public int id { get; set; }
    }
}
