﻿using richa_groceryproject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace richa_groceryproject.BusinessLayer.Interfaces
{
    public interface IGroceryServices
    {
        public List<Product> GetAllProducts();
        public List<Category> GetAllCategories();
        public ApplicationUser PlaceOrder(ApplicationUser app , int id);
        public Product GetProductById(int id);
        public List<ProductOrder> GetOrderInfo(int userId);

    }
}
