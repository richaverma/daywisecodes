﻿using richa_groceryproject.BusinessLayer.Interfaces;
using richa_groceryproject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace richa_groceryproject.BusinessLayer.Services
{
    public class GroceryServices : IGroceryServices
    {
        private readonly Grocery_DbContext _context;
       public GroceryServices(Grocery_DbContext context)
        {
            _context = context;
        }
        public List<Category> GetAllCategories()
        {
            return _context.Category.ToList();
        }

        public List<Product> GetAllProducts()
        {
           return _context.Product.ToList() ;
        }

        public List<ProductOrder> GetOrderInfo(int userId)
        {
           _context.ProductOrder.SingleOrDefault(x=>x.Userid ==userId);
            return _context.ProductOrder.ToList();
        }

        public Product GetProductById(int id)
        {
            return _context.Product.SingleOrDefault(x => x.Productid == id);
        }

        public ApplicationUser PlaceOrder(ApplicationUser app, int id)
        {
            return _context.ApplicationUser.SingleOrDefault(x => x.Id == id);
        }
    }
}
