﻿using System;
using System.Collections.Generic;

namespace richa_groceryproject.Models
{
    public partial class ProductOrder
    {
        public int Orderid { get; set; }
        public int Productid { get; set; }
        public int Userid { get; set; }

        public virtual Product Product { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
