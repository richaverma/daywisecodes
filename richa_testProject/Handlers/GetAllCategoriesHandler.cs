﻿using MediatR;
using richa_groceryproject.BusinessLayer.Interfaces;
using richa_groceryproject.Models;
using richa_groceryproject.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace richa_groceryproject.Handlers
{

    public class GetAllCategoriesHandler : IRequestHandler<GetAllCategoriesQueries, List<Category>>
    {
        private readonly IGroceryServices groceryServices;

        public GetAllCategoriesHandler(IGroceryServices groceryServices)
        {
            this.groceryServices = groceryServices;
        }

        public async Task<List<Category>> Handle(GetAllCategoriesQueries request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(groceryServices.GetAllCategories());
        }
    }
}
