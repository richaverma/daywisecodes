﻿using MediatR;
using richa_groceryproject.BusinessLayer.Interfaces;
using richa_groceryproject.Commands;
using richa_groceryproject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace richa_groceryproject.Handlers
{
    public class PlaceOrderHandler : IRequestHandler<PlaceOrderCommand, ApplicationUser>
    {
        private readonly IGroceryServices _data;

        public PlaceOrderHandler(IGroceryServices data)
        {
            _data = data;
        }

        public async Task<ApplicationUser> Handle(PlaceOrderCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.PlaceOrder(request.productid , request.userid));
        }
    }
}
