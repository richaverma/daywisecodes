﻿using MediatR;
using richa_groceryproject.BusinessLayer.Interfaces;
using richa_groceryproject.Models;
using richa_groceryproject.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace richa_groceryproject.Handlers
{
    public class GetAllProductsHandler : IRequestHandler<GetAllProductsQueries, List<Product>>
    {
        private readonly IGroceryServices groceryServices;
        public GetAllProductsHandler(IGroceryServices groceryServices)
        {
            this.groceryServices = groceryServices;
        }

        public async Task<List<Product>> Handle(GetAllProductsQueries request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(groceryServices.GetAllProducts());
        }
    }
}
