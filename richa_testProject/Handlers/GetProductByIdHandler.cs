﻿using MediatR;
using richa_groceryproject.BusinessLayer.Interfaces;
using richa_groceryproject.Models;
using richa_groceryproject.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace richa_groceryproject.Handlers
{
    public class GetProductByIdHandler : IRequestHandler<GetProductByIdQueries, Product>
    {
        private readonly IGroceryServices groceryServices;
public GetProductByIdHandler(IGroceryServices groceryServices)
        {
            this.groceryServices = groceryServices;
        }

        public async Task<Product> Handle(GetProductByIdQueries request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(groceryServices.GetProductById(request.id));
        }
    }
}
