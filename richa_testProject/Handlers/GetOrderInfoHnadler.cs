﻿using MediatR;
using richa_groceryproject.BusinessLayer.Interfaces;
using richa_groceryproject.Models;
using richa_groceryproject.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace richa_groceryproject.Handlers
{
    public class GetOrderInfoHnadler : IRequestHandler<GetOrderInfoQueries, List<ProductOrder>>
    {
        private readonly IGroceryServices groceryServices;

        public GetOrderInfoHnadler(IGroceryServices groceryServices)
        {
            this.groceryServices = groceryServices;
        }

        public async Task<List<ProductOrder>> Handle(GetOrderInfoQueries request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(groceryServices.GetOrderInfo(request.id));
        }
    }
}
