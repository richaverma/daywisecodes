﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using richa_groceryproject.Models;
using richa_groceryproject.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace richa_groceryproject.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class GroceryController : ControllerBase
    {
        private readonly ISender _sender;



        public GroceryController(ISender sender)
        {
            _sender = sender;
        }



        [HttpGet]
        public async Task<List<Product>> ViewAllProducts()
        {
            return await _sender.Send(new GetAllProductsQueries());
        }



        [HttpGet]
        public async Task<List<Category>> ViewListOfCategories()
        {
            return await _sender.Send(new GetAllCategoriesQueries());
        }



        [HttpGet]
        public async Task<Product> GetProductDetailsById(int productid)
        {
            return await _sender.Send(new GetProductByIdQueries() { id = productid });
        }



        [HttpGet("{id}")]
        public async Task<List<ProductOrder>> ShowOrderInfo(int userid)
        {
            return await _sender.Send(new GetOrderInfoQueries() { id = userid });
        }
        
        [HttpPost]
       public ProductOrder AddProductOrder(int productid, int userid)
        {
            var orderid = _context.ProductOrder.Max(x => x.OrderId) + 1;
            var product = _context.Product.SingleOrDefault(x => x.ProductId == productid);
            var user = _context.ApplicationUser.SingleOrDefault(x => x.UserId == userid);
            var productorder = new ProductOrder()
            {
                UserId = userid,
                ProductId = productid,
                OrderId = orderid,
                Product = product,
                User = user
            };
            _context.ProductOrder.Add(productorder);
            _context.SaveChanges();
            return productorder;
        }


    }
}
