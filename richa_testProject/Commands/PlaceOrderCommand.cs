﻿using MediatR;
using richa_groceryproject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace richa_groceryproject.Commands
{
    public class PlaceOrderCommand :IRequest<ApplicationUser>
    {
        public int productid { get; set; }
         public int userid { get; set; }
    }
}
