﻿using System;
using System.Collections;
using System.Collections.Generic;
namespace richa_collections2_12aug
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> mylist = new List<int>();

            // adding items in mylist
            for (int j = 5; j < 10; j++)
            {
                mylist.Add(j * 3);
            }

            // Displaying items of mylist
            // by using foreach loop
            foreach (int items in mylist)
            {
                Console.WriteLine(items);
            }
        }
    }
}
