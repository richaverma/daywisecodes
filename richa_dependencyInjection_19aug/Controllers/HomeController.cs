﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using richa_dependencyInjection_19aug.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace richa_dependencyInjection_19aug.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly DateTime _dateTime; 
        public HomeController(DateTime dateTime)
        {
            _dateTime = dateTime; 
        }

        public IActionResult Index()
        {
            var serverTime = _dateTime.Now; 
            if(serverTime.Hour < 12)
            {
                ViewData["Message"] = "It'smorning here";
            }
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
