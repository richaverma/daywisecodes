﻿using MVC_CQRS_UnitTesting_Project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC_CQRS_UnitTesting_Project.DataAccess
{
    public interface IDataAccess
    {
        public List<StudentTable> addStudent(StudentTable student);
        public List<StudentTable> getStudents();

        public StudentTable getStudent(int id);
    }
}
