﻿using MVC_CQRS_UnitTesting_Project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC_CQRS_UnitTesting_Project.DataAccess
{
    public class Dataccess : IDataAccess
    {
        public StudentdbContext data;
        public List<StudentTable> student = new List<StudentTable>() {
        new StudentTable{Id=1 , Name ="Richa" , Dept = "ECE" }
        };
        public Dataccess(StudentdbContext _data )
        {
            data = _data;
           
        }
        public List<StudentTable> addStudent(StudentTable student)
        {
            data.StudentTable.ToList().Add(student);
            return data.StudentTable.ToList();
        }

        public StudentTable getStudent(int id)
        {
            return data.StudentTable.ToList().SingleOrDefault(o => o.Id == id);
        }

        public List<StudentTable> getStudents()
        {
            return data.StudentTable.ToList();
        }
    }
}
