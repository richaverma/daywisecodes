﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MVC_CQRS_UnitTesting_Project.Models
{
    public partial class StudentTable
    {  [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Dept { get; set; }
    }
}
