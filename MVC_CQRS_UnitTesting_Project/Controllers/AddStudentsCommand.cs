﻿using MediatR;

namespace MVC_CQRS_UnitTesting_Project.Controllers
{
    internal class AddStudentsCommand : IRequest<object>
    {
        public object Employee { get; set; }
    }
}