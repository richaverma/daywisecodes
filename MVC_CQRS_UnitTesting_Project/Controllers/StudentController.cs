﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MVC_CQRS_UnitTesting_Project.Commands;
using MVC_CQRS_UnitTesting_Project.Models;
using MVC_CQRS_UnitTesting_Project.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC_CQRS_UnitTesting_Project.Controllers
{
    public class StudentController : Controller
    {
        private readonly ISender _sender;

        public StudentController(ISender sender)
        {
            _sender = sender;
        }
        public ViewResult Index()
        {
            return View() ;
        }
        public async Task<ViewResult> GetAllEmployees()
        {
            var empList = await _sender.Send(new GetStudentsQuery());
            return View(empList);
        }

        public async Task<ActionResult> GetById(int id)
        {
            var emp = await _sender.Send(new GetStudentByIdQuery() { id = id });
            return View(emp);
        }



        public async Task<ActionResult> AddEmployee(StudentTable employee)
        {
            await _sender.Send(new AddStudentCommand() { student = employee });
            return RedirectToAction("GetAllEmployees");
        }



       
    }
}
