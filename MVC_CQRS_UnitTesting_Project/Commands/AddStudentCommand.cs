﻿using MediatR;
using MVC_CQRS_UnitTesting_Project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC_CQRS_UnitTesting_Project.Commands
{
    public class AddStudentCommand :IRequest<List<StudentTable>>
    {
        public StudentTable student { get; set; }
        
    }
}
