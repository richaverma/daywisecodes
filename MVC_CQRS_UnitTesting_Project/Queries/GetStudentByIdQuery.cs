﻿using MediatR;
using MVC_CQRS_UnitTesting_Project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC_CQRS_UnitTesting_Project.Queries
{
    public class GetStudentByIdQuery :IRequest<StudentTable>
    {
        public int id { get; set; }
    }
}
