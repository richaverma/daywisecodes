﻿using MediatR;
using MVC_CQRS_UnitTesting_Project.Commands;
using MVC_CQRS_UnitTesting_Project.DataAccess;
using MVC_CQRS_UnitTesting_Project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MVC_CQRS_UnitTesting_Project.Handlers
{
    public class AddStudentsHandler : IRequestHandler<AddStudentCommand, List<StudentTable>>
    {
        private readonly IDataAccess _data;
        public AddStudentsHandler(IDataAccess data)
        {
            _data = data;
        }
        public async Task<List<StudentTable>> Handle(AddStudentCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.addStudent(request.student));
        }
    }
}
