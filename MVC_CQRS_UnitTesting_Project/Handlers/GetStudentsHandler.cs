﻿using MediatR;
using MVC_CQRS_UnitTesting_Project.DataAccess;
using MVC_CQRS_UnitTesting_Project.Models;
using MVC_CQRS_UnitTesting_Project.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MVC_CQRS_UnitTesting_Project.Handlers
{
    public class GetStudentsHandler : IRequestHandler<GetStudentsQuery, List<StudentTable>>
    {
        IDataAccess _data;

        public GetStudentsHandler(IDataAccess data)
        {
            _data = data;
        }

        public async Task<List<StudentTable>> Handle(GetStudentsQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.getStudents());
        }
    }
}
