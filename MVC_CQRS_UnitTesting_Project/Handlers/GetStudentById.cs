﻿using MediatR;
using MVC_CQRS_UnitTesting_Project.DataAccess;
using MVC_CQRS_UnitTesting_Project.Models;
using MVC_CQRS_UnitTesting_Project.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MVC_CQRS_UnitTesting_Project.Handlers
{
    public class GetStudentById : IRequestHandler<GetStudentByIdQuery, StudentTable>
    {
        IDataAccess data;

        public GetStudentById(IDataAccess data)
        {
            this.data = data;
        }

        public async Task<StudentTable> Handle(GetStudentByIdQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(data.getStudent(request.id));
        }
    }
}
