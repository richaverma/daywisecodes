﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using richa_webapi_crud_23aug.Models;

namespace richa_webapi_crud_23aug.Controllers
{
    public class TestempTablesController : Controller
    {
        private readonly testdbContext _context;

        public TestempTablesController(testdbContext context)
        {
            _context = context;
        }

        // GET: TestempTables
        public async Task<IActionResult> Index()
        {
            return View(await _context.TestempTable.ToListAsync());
        }

        // GET: TestempTables/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var testempTable = await _context.TestempTable
                .FirstOrDefaultAsync(m => m.EmpId == id);
            if (testempTable == null)
            {
                return NotFound();
            }

            return View(testempTable);
        }

        // GET: TestempTables/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TestempTables/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("EmpId,Name,Dept")] TestempTable testempTable)
        {
            if (ModelState.IsValid)
            {
                _context.Add(testempTable);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(testempTable);
        }

        // GET: TestempTables/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var testempTable = await _context.TestempTable.FindAsync(id);
            if (testempTable == null)
            {
                return NotFound();
            }
            return View(testempTable);
        }

        // POST: TestempTables/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("EmpId,Name,Dept")] TestempTable testempTable)
        {
            if (id != testempTable.EmpId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(testempTable);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TestempTableExists(testempTable.EmpId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(testempTable);
        }

        // GET: TestempTables/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var testempTable = await _context.TestempTable
                .FirstOrDefaultAsync(m => m.EmpId == id);
            if (testempTable == null)
            {
                return NotFound();
            }

            return View(testempTable);
        }

        // POST: TestempTables/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var testempTable = await _context.TestempTable.FindAsync(id);
            _context.TestempTable.Remove(testempTable);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TestempTableExists(int id)
        {
            return _context.TestempTable.Any(e => e.EmpId == id);
        }
    }
}
