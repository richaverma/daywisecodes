﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace richa_webapi_crud_23aug.Models
{
    public partial class TestempTable
    {  [Required]
        public int EmpId { get; set; }
       [Required]
        public string Name { get; set; }
       [Required]
        public string Dept { get; set; }
    }
}
