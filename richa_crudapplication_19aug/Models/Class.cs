﻿namespace richa_crudapplication_19aug.Models
{
    public class Student
    {
        public Guid StudentId { get; set; }
        public string StudentName { get; set; }
        public string Section { get; set; }
        public string Grade { get; set; }
        public string Gender { get; set; }
        public string DOJ { get; set; }
        public string DOB { get; set; }
        public string stream { get; set; }
        public string course { get; set; }

    }
}
