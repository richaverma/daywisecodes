﻿using CQRS_Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CQRS_Library.DataAccess
{
    public class DataAccess : IDataAccess
    {
        private List<StudentModel> students = new List<StudentModel>()
        {
            new StudentModel()
            {
                id =1,
                name = "Richa",
                age =21
            },
             new StudentModel()
            {
                id =1,
                name = "Harry",
                age =28
            }
        };
        public StudentModel addStudent(int id, string name, int age)
        {
            StudentModel stu  = new StudentModel() { id = id, name = name, age = age };
            students.Add(stu);
            return stu;
        }

        public StudentModel getStudent(int id)
        {
            return students.SingleOrDefault(c => c.id == id);
        }

        public List<StudentModel> getStudents()
        {
            return students;
        }
    }
}
