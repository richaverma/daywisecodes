﻿using CQRS_Library.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CQRS_Library.DataAccess
{
    public interface IDataAccess
    {
        public StudentModel addStudent(int id, string name, int age);
        public List<StudentModel> getStudents();

        public StudentModel getStudent(int id);
    }
}
