﻿using CQRS_Library.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace CQRS_Library.Queries
{
    public class GetstudentById :IRequest<StudentModel>
    {
        public int id;
    }

}
