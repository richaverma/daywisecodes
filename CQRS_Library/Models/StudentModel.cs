﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CQRS_Library.Models
{
    public class StudentModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public int age { get; set; }
    }
}
