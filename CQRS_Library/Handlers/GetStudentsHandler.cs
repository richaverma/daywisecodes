﻿using CQRS_Library.DataAccess;
using CQRS_Library.Models;
using CQRS_Library.Queries;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CQRS_Library.Handlers
{
    class GetStudentsHandler : IRequestHandler<Getstudents, List<StudentModel>>
    {
        private readonly IDataAccess _data;

        public GetStudentsHandler(IDataAccess idata)
        {
            _data = idata;
        }
        public Task<List<StudentModel>> Handle(Getstudents request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_data.getStudents());
        }
    }
}
