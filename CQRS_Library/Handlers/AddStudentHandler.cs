﻿using CQRS_Library.Commands;
using CQRS_Library.DataAccess;
using CQRS_Library.Models;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace CQRS_Library.Handlers
{
    internal class AddStudentHandler : IRequestHandler<AddStudent , StudentModel>
    {
        private readonly IDataAccess _data;
        public AddStudentHandler(IDataAccess data)
        {
            _data = data;
        }

        Task<StudentModel> IRequestHandler<AddStudent, StudentModel>.Handle(AddStudent request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_data.addStudent(request.id ,request.name ,request.age));
        }
    }
}
