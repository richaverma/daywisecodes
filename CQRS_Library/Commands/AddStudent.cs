﻿using CQRS_Library.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace CQRS_Library.Commands
{
    public class AddStudent: IRequest<StudentModel>
        {
          public  int id { get; set; }
       public  string name { get; set; }
       public int age { get; set; }
        }

}