﻿using System;

namespace richa_inheritance_11aug
{
    class Program
    {
        static void Main(string[] args)
        {
            triangle t1 = new triangle();
            triangle t2 = new triangle();
            t1.height = 12;
            t1.width = 8;
            t2.height = 16;
            t2.width = 5;
            t1.showdim();
            t1.showstyle();
        }
    }

    class shape
    {
        public int height;
        public int width;
        public void showdim()
        {
            Console.WriteLine("width and height are  : " + width + " and " + height);
        }
    }

    class triangle : shape
    {
        public string style;
        public double area()
        {
            return width * height / 2; 
        }

        public void showstyle()
        {
            Console.WriteLine(width);
        }
    }
}
