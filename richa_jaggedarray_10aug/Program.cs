﻿using System;

namespace richa_jaggedarray_10aug
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] mon = { "Mon" };
            string[] tue = { "Tue" };
            string[] wed = { "Wed" };

            string[][] day = { mon, tue, wed };

            foreach(var item in day)
            {
                Console.WriteLine(item[0]);
            }
        }
    }
}
