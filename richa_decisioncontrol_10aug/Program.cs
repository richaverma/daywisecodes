﻿// See https://aka.ms/new-console-template for more information
using System;

namespace richa_decisioncontrol_10aug
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = Convert.ToInt32(Console.ReadLine());
            if (num % 2 == 1) Console.WriteLine("It is an odd number");
            else Console.WriteLine("It is an even number");
        }
    }
}