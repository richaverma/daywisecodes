﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace richa_swaggerexp_24aug.Models
{
    public class Student
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string roll { get; set; }
    }
}
