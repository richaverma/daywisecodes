﻿using Microsoft.AspNetCore.Mvc;
using richa_swaggerexp_24aug.Interfaces;
using richa_swaggerexp_24aug.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace richa_swaggerexp_24aug.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        IStudentService _studentservice; 

        public StudentController(IStudentService studentService)
        {
            _studentservice = studentService; 
        }

        // GET: api/<StudentController>
        [HttpGet]
        public List<Student> Get()
        {
          return  _studentservice.Gets();
        }

        // GET api/<StudentController>/5
        [HttpGet("{id}")]
        public Student Get(int id)
        {
            return _studentservice.Get(id);
        }

        // POST api/<StudentController>
        [HttpPost]
        public List<Student> Post([FromBody] Student student)
        {
            return _studentservice.Save(student);
        }


        // PUT api/<StudentController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<StudentController>/5
        [HttpDelete("{id}")]
        public List<Student> Delete(int id)
        {
            return _studentservice.Delete(id);
        }
    }
}
