﻿using richa_swaggerexp_24aug.Interfaces;
using richa_swaggerexp_24aug.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace richa_swaggerexp_24aug.Services
{
    public class StudentService : IStudentService

    {
        List<Student> _student = new List<Student>() ;

        public StudentService()
        {
            for(int i=1; i<=9; i++)
            {
                _student.Add(new Student()
                {
                    id = i,
                    Name = "Stu" + i,
                    roll = "100" +i
                }); 
                 
                
            }
        }
        public List<Student> Delete(int studentid)
        {
             _student.RemoveAll(x => x.id == studentid);
            return _student;

        }

        public Student Get(int id)
        {
            return _student.SingleOrDefault(x => x.id == id);
        }

        public List<Student> Gets()
        {
            return _student;
        }

        public List<Student> Save(Student student)
        {
            _student.Add(student);
            return _student;
        }
    }
}
