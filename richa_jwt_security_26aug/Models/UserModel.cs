﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace richa_jwt_security_26aug.Models
{
    public class UserModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string EmailAddress { get; set; }
        public string Role { get; set; }
        public string Surname { get; set; }
        public string GivenName { get; set; }
    }
}
