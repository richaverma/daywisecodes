﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace richa_employeecrud_22aug.Models
{
    public class Employee
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public int Age { get; set; }
        public string Gender{ get; set; }
        public string Dept { get; set; }
    }
}
