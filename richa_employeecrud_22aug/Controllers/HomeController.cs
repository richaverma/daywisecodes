﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using richa_employeecrud_22aug.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace richa_employeecrud_22aug.Controllers
{
    public class HomeController : Controller
    {
        private static IList<Employee> emp = new List<Employee>
        {
            new Employee{Id=Guid.NewGuid(),Name="Krishna",Gender="Male" ,Age = 22 , Dept = "CSE"},
           new Employee{Id=Guid.NewGuid(),Name="Rama",Gender="Female" ,Age = 28 , Dept = "ECE"},
           new Employee{Id=Guid.NewGuid(),Name="Sneh",Gender="Male" ,Age = 29 , Dept = "CSE"},
            new Employee{Id=Guid.NewGuid(),Name="Shyam",Gender="Male" ,Age = 22 , Dept = "CSE"},
            new Employee{Id=Guid.NewGuid(),Name="John",Gender="Male" ,Age = 22 , Dept = "CSE"},
        };

        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult AddEmployee()
        {
            return View();
        }
        [HttpPost]
        public IActionResult AddEmployee(Employee input)
        {
            emp.Add(new Employee { Id = Guid.NewGuid(), Name = input.Name, Age = input.Age, Dept = input.Dept, Gender = input.Gender });
            //return View();
            return RedirectToAction("EmployeeList");
        }
        [HttpPost]
        public IActionResult Delete(Guid id)
        {
            emp.Remove(emp.SingleOrDefault(o => o.Id == id));
            ViewBag.message = "Employee deleted successfully.";
            return RedirectToAction("EmployeeList");
        }

        public ActionResult Update(Guid id)
        {
            var stud = emp.SingleOrDefault(o => o.Id == id);
            emp.Remove(stud);
            return RedirectToAction("AddEmployee");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
