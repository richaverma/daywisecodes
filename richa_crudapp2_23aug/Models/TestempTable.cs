﻿using System;
using System.Collections.Generic;

namespace richa_crudapp2_23aug.Models
{
    public partial class TestempTable
    {
        public int EmpId { get; set; }
        public string Name { get; set; }
        public string Dept { get; set; }
    }
}
