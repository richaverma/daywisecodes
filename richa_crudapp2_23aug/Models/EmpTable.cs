﻿using System;
using System.Collections.Generic;

namespace richa_crudapp2_23aug.Models
{
    public partial class EmpTable
    {
        public int Empid { get; set; }
        public string Ename { get; set; }
        public string Dept { get; set; }
    }
}
