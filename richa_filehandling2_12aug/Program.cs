﻿using System;
using System.IO;
namespace richa_filehandling2_12aug
{
    class Program
    {
        static void Main(string[] args)
        {
            filewrite wr = new filewrite();
            wr.readdata();
        }

        class filewrite
        {
            public void readdata()
            {
                FileStream fs = new FileStream("e:\\samplefile.txt", FileMode.Open, FileAccess.Read);
                StreamReader sw = new StreamReader(fs);
                Console.WriteLine("Content in sample file is : ");
                String line;
                while((line = sw.ReadLine()) != null)
                {
                    Console.WriteLine(line);
                }
               
                sw.Close();
                fs.Close();
            }
        }
    }
}
