﻿using System;

namespace richa_SI_9aug
{
    class Program
    {
        static void Main(string[] args)
        {
            double P = Convert.ToDouble(Console.ReadLine());
            double R = Convert.ToDouble(Console.ReadLine());
            double T = Convert.ToDouble(Console.ReadLine());

            double SI = P * R * T / 100;
            Console.WriteLine(SI); 
        }
    }
}
