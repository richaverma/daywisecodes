﻿using System;

namespace richa_generics_12aug
{
    public class someclass
    {
        public void genericmethod<T1 , T2>(T1 param1 , T2 param2)
        {
            Console.WriteLine($"Parameter 1 : {param1} : Prameter 2 : {param2}");
        }
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Generics Method Example");
            someclass s = new someclass();
            //while calling the method we need to specify the datatype
            s.genericmethod<int , int>(10, 10);
            s.genericmethod<String , char>("Hii", 'T');

        }
    }
}
