﻿using System;
using System.Threading; 
namespace richa_thread_12aug
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread t1 = new Thread(new ThreadStart(work1));
            Thread t2 = new Thread(new ThreadStart(work2));
            t1.Start();
            t2.Start();
        }
        static void work1()
        {
            for(int i = 1; i<=5; i++)
            {
                Console.WriteLine("Thread 1 is running for " + Convert.ToString(i) + " time");
            }
        }

        static void work2()
        {
            for(int i = 1; i<=5; i++)
            {
                Console.WriteLine("Thread 2 is running for " + Convert.ToString(i) + " time");
            }
        }
    }
}
