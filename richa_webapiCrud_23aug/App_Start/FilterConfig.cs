﻿using System.Web;
using System.Web.Mvc;

namespace richa_webapiCrud_23aug
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
