﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace richa_dbconnection_22aug.Models
{
    public partial class EmpTable
    {
       [Required]
        public int Empid { get; set; }
        [Required]
        public string Ename { get; set; }
       [Required]
        public string Dept { get; set; }
    }
}
