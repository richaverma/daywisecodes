﻿using System;

namespace richa_abstarctclass_11aug
{
    class Program
    {
        static void Main(string[] args)
        {
            car carobj = new car();
            carobj.display();
            student studentobj = new student();
            studentobj.display();
            books booksobj = new books();
            booksobj.display();
        }

       abstract class vehicle
        {
            public abstract void display(); 
        }

        class car  : vehicle
        {
            public override void display()
            {
                Console.WriteLine("This is overriden display method in car class");
            }
        }

        abstract class Class
        {
            public abstract void display();
        }

        class student : Class
        {
            public override void display()
            {
                Console.WriteLine("This is overriden display method in student class");
            }
        }

        abstract class lib
        {
            public abstract void display();
        }

        class books : lib
        {
            public override void display()
            {
                Console.WriteLine("This is overriden display method in books class");
            }
        }

    }
    }

