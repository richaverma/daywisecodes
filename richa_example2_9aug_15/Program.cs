﻿using System;

namespace richa_basicexample_aug9_14
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Please enter the 1st number");
            int firstval, secondval;
            firstval = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Please enter the 2nd number");
            secondval = Convert.ToInt32(Console.ReadLine());
            int res = firstval - secondval;
            Console.WriteLine(res);
        }
    }
}
