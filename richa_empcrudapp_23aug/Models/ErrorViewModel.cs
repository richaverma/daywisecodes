using System;

namespace richa_empcrudapp_23aug.Models
{
    public class ErrorViewModel
    {  
        public string RequestId { get; set; }
        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}
